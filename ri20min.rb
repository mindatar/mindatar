class MegaAnfitrion
    attr_accessor :nombres
#Crear el objeto
    def initialize(nombres="Mundo")
        @nombres = nombres
    end
    
    #Decirles HOla a Todos
    def decir_hola
        if @nombres.nil?
           puts "...."
        elsif @nombres.respond_to?("each")
           #@nombres es una lista de algún tipo,
           #Asi que podemos iterar
           @nombres.each do |nombre| 
               puts "HOlas #{@nombres.join(", ")}. Vuelvan pronto."
           end
        else
           puts "Holas #{@nombres}.Vuele pronto."

        end
     end


# Decirle adiós a todos
  def decir_adios
    if @nombres.nil?
      puts "..."
    elseif @nombres.respond_to?("join")
      # Juntar los elementos de la lista
      # usando la coma como separador
      puts "Adiós #{@nombres.join(", ")}. Vuelvan pronto."
    else
      puts "Adiós #{@nombres}. Vuelve pronto."
    end
  end

end

if __FILE__ == $0
    ma = MegaAnfitrion.new
    ma.decir_hola
    ma.decir_adios

# Cambiar el nombre a "Diego" 
    ma.nombres = "Diego"
    ma.decir_hola 
    ma.decir_adios

# Cambiar el nombre a un vector de nombres
    ma.nombres = ["Albert", "Beatriz","Carlos","David","Ernesto"]
    ma.decir_hola
    ma.decir_adios

# Cambiarlo a nil
    ma.nombres =nil
    ma.decir_hola
    ma.decir_adios

end

